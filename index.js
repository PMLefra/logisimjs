function setup() {
  createCanvas(windowWidth, windowHeight);

  for (let i = 0; i < Math.floor(width / Cell.width); i++) {
    Cell.grid[i] = [];
    for (let j = 0; j < Math.floor(height / Cell.height);  j++)
      Cell.grid[i].push(new Cell(i, j));
  }
  
  background(51);
  noStroke();

  for (let col of Cell.grid)
    for (let cell of col)
      cell.render();
}

function mouseClicked() {
  const i = Math.floor(mouseX / Cell.width), j = Math.floor(mouseY / Cell.height);
  const cell = Cell.get(i, j);

  if (mouseButton == 0)
    Cell.set(i, j, Cell.selectedType);
  else if (mouseButton == 2) {
    if (cell.type == "empty") Cell.set(i, j, "cable");
    else Cell.set(i, j, "empty");
  }
}

// TODO: Modifier ldl.js pour y ajouter cet event :p
canvas.addEventListener("auxclick", e => {
  if (e.button == 1) {
    const i = Math.floor(mouseX / Cell.width), j = Math.floor(mouseY / Cell.height);
    const cell = Cell.get(i, j);
    if (cell.type !== "switch") return;

    cell.power = !cell.power;
    cell.updateNeighbors();
    cell.update();
  }
});
