class Switch extends CableLike {
  power = false;
  type = "switch";
  
  constructor(i, j) {
    super(i, j);
  }

  update() {
    this.updateConnections();
    this.updateSectionPower();
    this.render();
  }

  render() {
    this.renderClear();

    if (this.power) fill(60, 200, 60);
      else fill(25, 125, 25);
    
    const quarterW = Math.round(Cell.width / 4),
      quarterH = Math.round(Cell.height / 4),
      halfW = Math.round(Cell.width / 2),
      halfH = Math.round(Cell.height / 2);
    
    rect(this.pos.x + quarterW, this.pos.y + quarterH, halfW, halfH);
    fill(100);
    this.renderConnections();
    fill(200, 200, 200);
    ellipse(this.pos.x + halfW, this.pos.y + halfH, quarterW, quarterH);
  }
}

Cell.registerCellType("switch", Switch);
