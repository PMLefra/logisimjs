class CableLike extends Cell {
  // Requires a full section (not only the cables)
  static isPowered(fSection) {
    for (let cableLike of fSection)
      if (cableLike.power) return true;
    
    return false;
  }

  // Requires a full section (not only the cables)
  static updateSectionPower(fSection) {
    const isPowered = CableLike.isPowered(fSection);
    
    for (let cableLike of fSection) {
      const oldState = cableLike.active;

      if (cableLike.type === "cable") cableLike.active = isPowered;
    
      // This special case is important:
      // if one of the neighboring sections of the output is still active, it shouldn't be deactivated!
      else if (cableLike.type === "output") {
        let neighbors = cableLike.getNeighbors(true, true);
    
        for (let neighbor of Object.values(neighbors)) {
          if (neighbor instanceof CableLike && neighbor.isPowered()) {
            cableLike.active = true;
            break;
          }
          cableLike.active = false;
        }
      }

      if (oldState !== cableLike.active) cableLike.render();
    } 
  }
  
  connected = {
    top: false,
    left: false,
    bottom: false,
    right: false
  };

  constructor(i, j) {
    super(i, j);
  }

  getSection(onlyCables = false) {
    let section = [];

    function checkCell(cell) {
      if (section.includes(cell)) return;
      if ((onlyCables && cell.type !== "cable") || (!onlyCables && !(cell instanceof CableLike))) 
        return;

      section.push(cell);

      const neighbors = cell.getNeighbors(false, true);

      if (cell.type === "cable")
        for (const neighbor of neighbors)
          checkCell(neighbor);
    }

    checkCell(this);
    
    return section;
  }

  isPowered() {
    return (CableLike.isPowered(this.getSection()));
  }
  
  updateSectionPower() {
    // If a bridge is being updated, update every section around it, but not itself (useless)
    if (this.type === "bridge") {
      for (let neighbor of this.getNeighbors(false, true))
        if (neighbor instanceof CableLike)
          neighbor.updateSectionPower();
      return;
    }
    
    CableLike.updateSectionPower(this.getSection());
  }

  updateConnections() {
    const neighbors = this.getNeighbors(true);
    this.connected = {
      left: neighbors?.left instanceof CableLike,
      right: neighbors?.right instanceof CableLike,
      top: neighbors?.top instanceof CableLike,
      bottom: neighbors?.bottom instanceof CableLike
    }
  }

  renderConnections() {
    const quarterW = Math.round(Cell.width / 4),
      quarterH = Math.round(Cell.height / 4),
      halfW = Math.round(Cell.width / 2),
      halfH = Math.round(Cell.height / 2);
      
    if (this.connected.left)
      rect(this.pos.x, this.pos.y + quarterH, quarterW + 1, halfH);
    if (this.connected.right)
      rect(this.pos.x + 3 * quarterW, this.pos.y + quarterH, quarterW + 1, halfH);
    if (this.connected.top)
      rect(this.pos.x + quarterW, this.pos.y, halfW, quarterH + 1);
    if (this.connected.bottom)
      rect(this.pos.x + quarterW, this.pos.y + 3 * quarterH, halfW, quarterH + 1);
  }

  render() {
    super.render();
  }
}
