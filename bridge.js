class Bridge extends CableLike {
  type = "bridge";

  update() {
    this.updateConnections();
    this.updateSectionPower();
    this.render();
  }

  render() {
    this.renderClear();

    fill(90);
    
    const quarterW = Math.round(Cell.width / 4),
      quarterH = Math.round(Cell.height / 4),
      halfW = Math.round(Cell.width / 2),
      halfH = Math.round(Cell.height / 2);
    
    rect(this.pos.x + quarterW, this.pos.y + quarterH, halfW, halfH);

    fill(0);
    this.renderConnections();
  }
}

Cell.registerCellType("bridge", Bridge);
