class Gate {
  static width = 2.8 * Cell.width;
  static height = 2.8 * Cell.height;
  static list = [];

  pos;
  index;

  constructor(i, j) {
    this.index = createVector(i, j);
    this.pos = createVector((i + 0.7) * Cell.width, (j + 0.1) * Cell.height);
    Gate.list.push(this);
  }

  render() {
    
  }
}
