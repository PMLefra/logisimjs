class Constant extends CableLike {
  type = "constant";
  power = true;

  constructor(i, j) {
    super(i, j);
  }

  update() {
    this.updateConnections();
    this.updateSectionPower();
    this.render();
  }

  render() {
    this.renderClear();
    
    const quarterW = Math.round(Cell.width / 4),
      quarterH = Math.round(Cell.height / 4),
      halfW = Math.round(Cell.width / 2),
      halfH = Math.round(Cell.height / 2);
    
    fill(60, 200, 60);
    rect(this.pos.x + quarterW, this.pos.y + quarterH, halfW, halfH);
    this.renderConnections();
    fill(60, 60, 200);
    ellipse(this.pos.x + halfW, this.pos.y + halfH, quarterW, quarterH);
  }
}

Cell.registerCellType("constant", Constant);
