class Cell {
  static width = 28;
  static height = 28;
  static grid = [];
  static types = {
    "empty": Cell
  }
  static selectedType = "empty";

  static registerCellType(type, class_) {
    Cell.types[type] = class_;
  }

  static get(i, j) {
    if (i instanceof Vector) return Cell.grid[i.x][i.y];
    else return Cell.grid[i][j];
  }

  static set(i, j, type) {
    if (i instanceof Vector) {
      [i, j, type] = [i.x, i.y, j];
    }

    let oldType = Cell.grid[i][j].type;

    Cell.grid[i][j] = new Cell.types[type](i, j);
    Cell.grid[i][j].updateNeighbors();
    Cell.grid[i][j].update();
  }

  index;
  pos;
  type = "empty";

  constructor(i, j) {
    if (i instanceof Vector) this.index = i;
    else this.index = createVector(i, j);
    this.pos = createVector(this.index.x * Cell.width, this.index.y * Cell.height);
  }

  getNeighbors(sides = false, crossBridges = false) {
    let neighbors = {};

    if (this.index.x > 0)
      neighbors.left = Cell.get(this.index.x - 1, this.index.y);
    if (this.index.x < Cell.grid.length - 1)
      neighbors.right = Cell.get(this.index.x + 1, this.index.y);
    if (this.index.y > 0)
      neighbors.top = Cell.get(this.index.x, this.index.y - 1);
    if (this.index.y < Cell.grid[0].length - 1)
      neighbors.bottom = Cell.get(this.index.x, this.index.y + 1);

    if (crossBridges) {
      while (neighbors?.left?.type === "bridge" && neighbors?.left?.index?.x > 0)
        neighbors.left = Cell.get(neighbors.left.index.x - 1, neighbors.left.index.y);
      while (neighbors?.right?.type === "bridge" && neighbors?.right?.index?.x < Cell.grid.length - 1)
        neighbors.right = Cell.get(neighbors.right.index.x + 1, neighbors.right.index.y);
      while (neighbors?.top?.type === "bridge" && neighbors?.top?.index?.y > 0)
        neighbors.top = Cell.get(neighbors.top.index.x, neighbors.top.index.y - 1);
      while (neighbors?.bottom?.type === "bridge" && neighbors?.bottom?.index?.y < Cell.grid[0].length - 1)
        neighbors.bottom = Cell.get(neighbors.bottom.index.x, neighbors.bottom.index.y + 1);
    }

    if (sides) return neighbors;
    else return Object.values(neighbors);
  }

  update() {
    this.render();
  }

  updateNeighbors(callerCell) {
    for (let neighbor of this.getNeighbors())
      neighbor.update();
  }

  render() {
    if (this.type == "empty") {
    this.renderClear();
    fill(70);
    const quarterW = Math.round(Cell.width / 4),
      quarterH = Math.round(Cell.height / 4),
      halfW = Math.round(Cell.width / 2),
      halfH = Math.round(Cell.height / 2);
    
    ellipse(this.pos.x + halfW, this.pos.y + halfH, quarterW, quarterH);
    }

    for (let gate of Gate.list) gate.render();
  }

  renderClear() {
    fill(51);
    rect(this.pos.x, this.pos.y, Cell.width, Cell.height);
  }
}
