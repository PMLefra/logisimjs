class AndGate extends Gate {
  constructor(i, j) {
    super(i, j);
  }
  
  render() {
    push();
    fill(230);
    stroke(20);
    strokeWeight(5);
    
    ctx.beginPath();
    ctx.moveTo(this.pos.x, this.pos.y);
    ctx.bezierCurveTo(
      this.pos.x + 2.5 * Cell.width,
      this.pos.y - 5, this.pos.x + 2.5 * Cell.width,
      this.pos.y + Gate.height + 5, this.pos.x,
      this.pos.y + Gate.height
    );
    ctx.lineTo(this.pos.x, this.pos.y);
    ctx.stroke();
    ctx.fill();
    
    pop();
  }  
}
