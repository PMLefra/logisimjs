// TODO: Redo all of this, it's very sketchy since it's taken directly from the old version

document.oncontextmenu = () => false;

const selectedBorderColor = "rgba(66, 134, 244)";
const xOffset = 717, yOffset = 40;
const domElts = {
  empty: document.getElementById("empty"),
  bridge: document.getElementById("bridge"),
  output: document.getElementById("output"),
  switch: document.getElementById("switch"),
  constant: document.getElementById("constant"),
  and: document.getElementById("and"),
  or: document.getElementById("or"),
  xor: document.getElementById("xor"),
  not: document.getElementById("not"),
  move: document.getElementById("move"),
  menu: document.getElementById("menu")
};

function selectInMenu(type) {
  if (domElts.hasOwnProperty(Cell.selectedType))
    domElts[Cell.selectedType].style.borderColor = "gray";
  if (domElts.hasOwnProperty(type))
    domElts[type].style.borderColor = selectedBorderColor;
  Cell.selectedType = type;
}

for (let key in domElts) {
  if (key == "menu") continue;
  
  domElts[key].draggable = false;
  domElts[key].ondragstart = () => false;

  if (key == "move") continue;

  domElts[key].addEventListener("click", () => {
    selectInMenu(key);
  });
  
  domElts[key].addEventListener("mouseenter", () => {
    domElts[key].style.borderColor = "white";
  });
  
  domElts[key].addEventListener("mouseleave", () => {
    domElts[key].style.borderColor = key == Cell.selectedType ? selectedBorderColor : "gray";
  });
}

function moveMenu(e) {
  domElts["menu"].style.margin = "0";
  domElts["menu"].style.left = e.clientX - xOffset + "px";
  domElts["menu"].style.top = e.clientY - yOffset + "px";
}

domElts["move"].addEventListener("mousedown", e => {
  domElts["move"].style.backgroundColor = "rgba(80, 220, 80, 0.5)";
  window.addEventListener("mousemove", moveMenu);
});

domElts["move"].addEventListener("mouseup", (e) => {
  domElts["move"].style.backgroundColor = "";
  window.removeEventListener("mousemove", moveMenu);
});

selectInMenu("empty");
