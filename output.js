class Output extends CableLike {
  type = "output";
  active = false;

  constructor(i, j) {
    super(i, j);
  }

  update() {
    this.updateConnections();
    this.updateSectionPower();
    this.render();
  }

  render() {
    this.renderClear();

    if (this.active) fill(252, 184, 58);
      else fill(214, 154, 107);
    
    const quarterW = Math.round(Cell.width / 4),
      quarterH = Math.round(Cell.height / 4),
      halfW = Math.round(Cell.width / 2),
      halfH = Math.round(Cell.height / 2);
    
    rect(this.pos.x + quarterW, this.pos.y + quarterH, halfW, halfH);
    fill(100);
    this.renderConnections();
  }
}

Cell.registerCellType("output", Output);
